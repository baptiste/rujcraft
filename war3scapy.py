from scapy.all import *

class War3Packet(Packet):
    def post_build(self, p, pay):
        if self.len is None:
            p = p[:2] + struct.pack("<H", len(p)) + p[4:]
        return p


class War3SearchGame(War3Packet):
    name = "War3 Search Game "
    fields_desc=[XByteField("magic", 0xf7),
                 XByteField("type", 0x2f),
                 LEShortField("len", None),
                 StrFixedLenField("product", "3RAW", 4),
                 LEIntField("version", 26),
                 LEIntField("unknown", 0),
    ]


class War3GameInfo(War3Packet):
    name = "War3 Game Info "
    fields_desc=[XByteField("magic", 0xf7),
                 XByteField("type", 0x30),
                 LEShortField("len", None),
                 StrFixedLenField("product", "3RAW", 4),
                 LEIntField("version", 26),
                 LEIntField("gameId", 1),
                 LEIntField("unknown1", 0),
                 # Maximum length: 31 bytes (without counting the final \0)
                 StrNullField("name", ""),
                 XByteField("unknown2", 0),
                 # No idea of how this is formatted :(
                 StrNullField("stat", ""),
                 LEIntField("totalSlots", 4),
                 # This is documented as an array of bytes on
                 # https://redux.bnetdocs.org/?op=packet&pid=435
                 LEIntField("unknown3", 9),
                 LEIntField("currentPlayers", 1),
                 LEIntField("playerSlots", 4),
                 LEIntField("createdSince", 0),
                 LEShortField("gamePort", 6112),
    ]


class War3CreateGame(War3Packet):
    name = "War3 Create Game "
    fields_desc=[XByteField("magic", 0xf7),
                 XByteField("type", 0x31),
                 LEShortField("len", None),
                 StrFixedLenField("product", "3RAW", 4),
                 LEIntField("version", 26),
                 LEIntField("gameId", 1),
    ]


class War3RefreshGame(War3Packet):
    name = "War3 Refresh Game "
    fields_desc=[XByteField("magic", 0xf7),
                 XByteField("type", 0x32),
                 LEShortField("len", None),
                 LEIntField("gameId", 1),
                 LEIntField("currentPlayers", 1),
                 LEIntField("playerSlots", 4),
    ]
