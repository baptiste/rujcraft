#!/usr/bin/env python3

"""A simple proxy client for Warcraft III, using IP spoofing for direct
connection between two Warcraft III games.

It connects to a simple proxy server over TCP and uses this connection to
relay Warcraft III messages.

However, when sending game info messages to the local Warcraft III game,
it spoofs the public IP address of the server, so that Warcraft III will
connect directly to the server.

The server needs to be reachable on TCP ports 5555 (for meta connection)
and 6112 (for war3).

In addition, if you want to host a Warcraft III game on the client side,
you need to ensure that your Warcraft III is reachable from the Internet
on TCP port 6112.
"""

import argparse
import logging
import selectors
import socket
import struct
import time

from simpleproxy_common import SimpleProxy, META_PORT, LOGGING_FORMAT, LOGGING_DATEFMT


class SimpleProxyClient(SimpleProxy):

    def __init__(self, server_name, server_port):
        super().__init__()
        self.server_name = server_name
        self.server_port = server_port
        logging.info("Connecting to server at {} port {}".format(server_name, server_port))
        self.server_sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.server_sock.connect((server_name, server_port))
        logging.info("Connected!")
        self.server_ip = self.server_sock.getpeername()[0]
        self.select.register(self.server_sock, selectors.EVENT_READ, self.peer_read_tcp)
        # Keep track of message being received from server
        self.msg_len = 0
        self.msg = None

    def reconnect(self):
        # Cleanup
        self.select.unregister(self.server_sock)
        self.server_sock.close()
        self.server_sock = None
        self.server_ip = None
        self.msg = None
        self.msg_len = 0
        # Try to reconnect
        delay = 1
        while True:
            time.sleep(delay)
            self.server_sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            try:
                self.server_sock.connect((self.server_name, self.server_port))
            except socket.error as e:
                delay *= 2
                if delay <= 64:
                    logging.info('Failed with "{}", trying again in {} seconds...'.format(e, delay))
                    continue
                else:
                    logging.error("Giving up reconnecting")
                    exit(-1)
            logging.info("Reconnected!")
            return

    def peer_read_tcp(self, sock, mask):
        if self.msg == None:
            # New message
            try:
                header = sock.recv(2)
            except socket.error as e:
                logging.error('Got "{}" while reading from server (bitmask was {}), reconnecting'.format(e, mask))
                self.reconnect()
                return
            if not header:
                logging.error("Lost connection to server, reconnecting...")
                self.reconnect()
                return
            self.msg_len = struct.unpack("!H", header)[0]
            self.msg = b""
        # Continue receiving a message
        data = sock.recv(self.msg_len - len(self.msg))
        if not data:
            logging.error("Lost connection to server, reconnecting...")
            self.reconnect()
            return
        self.msg += data
        if len(self.msg) == self.msg_len:
            self.handle_peer_msg(self.msg, self.server_ip)
            self.msg = None
            self.msg_len = 0

    def send_msg_to_peers(self, msg):
        # Encoding: 2 bytes with length in network bytes order, then data
        length = struct.pack("!H", len(msg))
        logging.debug("Forwarding to server")
        self.server_sock.send(length + msg)


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument("server_name",
                        help="DNS name or IP of proxy server to connect to")
    parser.add_argument("-p", "--server-port", default=META_PORT,
                        help="TCP port of the proxy server (default: %(default)s)")
    parser.add_argument("-v", "--verbose", action="store_true",
                        help="Run in verbose mode")
    return parser.parse_args()

def main():
    args = parse_args()
    loglevel = logging.INFO
    if args.verbose:
        loglevel = logging.DEBUG
    logging.basicConfig(format=LOGGING_FORMAT, level=loglevel, datefmt=LOGGING_DATEFMT)
    proxy = SimpleProxyClient(args.server_name, args.server_port)
    proxy.main_loop()

if __name__ == '__main__':
    main()
