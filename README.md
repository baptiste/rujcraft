# Goal

Play LAN games of Warcraft III over the Internet without any need for port
redirect or VPN.  Overall, in Warcraft III, it feels just like all players
are on the same LAN!

It is inspired by [JurCraft](https://www.michaljodko.com/jurcraft/) but
takes the concept one step further by introducing a relaying server.

Just like [JurCraft](https://www.michaljodko.com/jurcraft/), each player
needs to run a small client.  This client needs to be run either on the
same machine as Warcraft III, or on another machine in the same LAN.

In addition, the system needs a server (called ServerProxy) that relays
game metadata and data between connected clients.  The ServerProxy does
not actually host a Warcraft III game: one of the clients still needs to
create a LAN game and wait for other clients to connect.

Any number of clients can be connected to the same ServerProxy, and
several LAN games can be running at the same time on a single ServerProxy.
The system supports any version of Warcraft III and The Frozen Throne, but
of course clients need to run the same version to be able to see each
other and connect.

Currently, all clients are able to see all LAN games hosted by other
clients (provided they use the same version of Warcraft III).  In the
future, it may be possible to specify a "realm" so that only clients with
the same realm are able to see each other's games (the realm acts like a
password or shared secret).

# Usage

This project needs coding first :)

# Architecture

## Overview

War3 client -- ClientProxy --(internet)-- ServerProxy --(internet)-- ClientProxy -- War3 client

The Client Proxy can run on the same machine as the Warcraft III client,
or on another machine in the same LAN.  It relays messages between the
local Warcraft III client and remote Warcraft III clients through the
ServerProxy.  The Warcraft III client itself does not need any
modification, just like with
[JurCraft](https://www.michaljodko.com/jurcraft/).

The ServerProxy coordinates ClientProxies and allows them to connect even
if they are all behind NAT.  However, the ServerProxy needs to be
reachable from all ClientProxies on a few TCP ports, so it needs to have a
public IP address or to be configured with port forwards.

A single ServerProxy can support any number of simultaneous ClientProxy.
Whenever a Warcraft III client starts a LAN game, the information is
forwarded by its local ClientProxy to the ServerProxy, which then
broadcasts it to all connected ClientProxies.  Each ClientProxy then sends
the information to its local Warcraft III client.  Thus, all Warcraft III
clients can see the new LAN game and can decide to connect.

When a Warcraft III client decides to connect to a LAN game hosted by a
remote Warcraft III, we setup 4 dedicated TCP connections in a chain to
relay the game traffic:

War3 client --[TCP1]-- ClientProxy1 --[TCP2]-- TURNServer --[TCP3]-- ClientProxy2 --[TCP4]-- War3 LAN host

The relaying server is a TURN server, which typically runs on the same
machine as the ServerProxy (but it can be a separate machine).

## Metadata connections

Each ClientProxy maintains a persistent TCP connection to the ProxyServer:
we call it the **metadata connection**.

On this connection, several families of messages are exchanged:

- game state management: search for a game, advertise a new game...
- coordination: help establishing data connections when a client wants to
  connect to a game

### Game state management

TODO: describe how we identify games uniquely

TODO: describe message types

### Coordination

TODO


## Game management on ServerProxy and ClientProxy

Each time a game is created by a ClientProxy, the ServerProxy assigns it a
**unique TCP game port** before broadcasting the information to all
ClientProxies.  This is necessary because all games will be visible by all
ClientProxies, and they need to have a way to distinguish them whenever
the Warcraft III client decides to connect to one of the games.

A ProxyClient knows about two types of games:

1. **local games:** games created by a local Warcraft III client,
   identified by the LAN IP address of the Warcraft III computer and its
   TCP port (most likely the standard 6112 port) **but also** by its
   unique TCP game port assigned by the server
2. **remote games:** games created by a remote Warcraft III client,
   identified by the unique game port assigned by the server

## State

### ClientProxy



### ServerProxy


## Events

### ClientProxy learns about a new local game

This happens when a local Warcraft III client on the LAN creates a game
(UDP broadcast on the LAN).

The ClientProxy forwards this information to the ServeryProxy.

In addition, it setups a **control connection to the TURN server** as
described in the dedicated TURN section below.

### ClientProxy learns about a new remote game

When a ClientProxy learns about a new remote game from the ServerProxy, it
broadcasts on the local LAN so that any Warcraft III client running in the
LAN learns about the game.  Among other information, this broadcast
contains the **unique TCP game port** assigned to the game by the
ServerProxy.

In addition, the ClientProxy binds a listening TCP socket on its LAN
address, on the **unique TCP game port**.  This will allow Warcraft III
clients on the LAN to connect to the game.

### Local Warcraft III client wants to connect to a remote game

This happens when the player joins a game from the "LAN games" menu.

This causes the Warcraft III client to open a TCP connection to its local
ClientProxy on the **unique TCP game port**.
  
The ClientProxy then sets up a data connection to the remote game server
using TURN (see below).  Once this data connection is setup, the
ClientProxy simply relays data between the Warcraft III client and the
data connection to the remote game server.

### Remote ClientProxy wants to connect to a local game




## Setting up data connections with TURN

For relaying actual game data, we use a TURN server running over TCP (see
[RFC6062](https://tools.ietf.org/html/rfc6062)).  The TURN protocol
provides the necessary pieces to relay traffic between two clients that
are behind a NAT.  In addition, high-performance TURN servers are already
available, for example [coturn](https://github.com/coturn/coturn) can
handle thousands of simultaneous connections.

The following figure illustrates what we want to achieve.  The Warcraft
III client on the left wants to connect to the Warcraft III host on the
right.  Arrows indicate in which direction the TCP connection needs to be
established because of NAT.

War3 client --[TCP1]--> ClientProxy1 --[TCP2]--> TURNServer <--[TCP3]-- ClientProxy2 --[TCP4]--> War3 LAN host

Note that each client-server Warcraft III interaction needs such a
dedicated chain of TCP connections.  Thus, ClientProxy2 will actually
handle several pairs of TCP3/TCP4 connections: one for each connecting
remote client.

### Preparation: ClientProxy2 learns about a new local game

When the ClientProxy learns about a new local game, it needs to open a
**control connection** to the TURN server.  This will later allow remote
clients to join the game through the TURN server.

### Connection establishment: ClientProxy1 wants to connect to ClientProxy2

This is a bit complicated because the TURN server needs a written
permission by ClientProxy2 before it accepts connections from ClientProxy1.

1. ClientProxy1 tells ClientProxy2 (through the ServerProxy) that it wants to connect
2. ClientProxy2 tells the TURN server (through its control connection)
   that ClientProxy1's IP address should be allowed to connect, using a
   `CreatePermission` TURN message
3. ClientProxy2 tells ClientProxy1 (through the ServerProxy) that it is
   now allowed to connect

Then the normal TURN process can take place (see
[RFC6062](https://tools.ietf.org/html/rfc6062)):

1. ClientProxy1 connects to the TURN server on the designed TCP port
2. the TURN server verifies that ClientProxy1's IP address is allowed
3. the TURN server notifies ClientProxy2 (using a `ConnectionAttempt
   Indication` TURN message on the control connection) that a peer wants
   to connect
4. ClientProxy2 creates a new TCP connection to the TURN server and sends
   a `ConnectionBind` request on it
5. the TURN server associates the TCP connection from ClientProxy1 and the
   TCP connection from ClientProxy2 and starts relaying data between themq


