#!/usr/bin/env python3

"""A simple proxy server for Warcraft III, using IP spoofing for direct
connection between two Warcraft III games.

It waits for incoming TCP connections from simple proxy clients, and will
use these connections to relay Warcraft III messages generated from a game
running on the local network.  It only relays control messages ("new
game", "searching for game", etc), not the actual multi-player gaming
connection used after a player joins a game.

Note: this server will *not* relay Warcraft III messages from one proxy
client to another.  It means that the person running the proxy server
should be the one hosting Warcraft III games.  Anybody else running a
proxy client *can* create games, but they will only be visible to the
person running the proxy server.  This is why it's called a "simple" proxy :-)

You need to ensure this server is reachable on TCP port 5555 from the
Internet, and that Warcraft III is reachable on TCP port 6112.

"""

import logging
import selectors
import socket
import struct
import sys

from simpleproxy_common import SimpleProxy, META_PORT, LOGGING_FORMAT, LOGGING_DATEFMT


class SimpleProxyServer(SimpleProxy):

    def __init__(self):
        super().__init__()
        # Setup passive socket to receive client connections over TCP
        self.passive_sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.passive_sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self.passive_sock.bind(('0.0.0.0', META_PORT))
        self.passive_sock.listen()
        logging.info("Listening for client connections on TCP port %d", META_PORT)
        self.select.register(self.passive_sock, selectors.EVENT_READ, self.new_tcp_client)
        # Data structures to manage clients.
        # Type: client socket -> (addr, port)
        self.clients = dict()
        # Message currently being received from a client.
        # Type: client socket -> bytes
        self.client_msg = dict()
        # Holds the expected length of the message being received from a client.
        # Type: client socket -> int
        self.client_msg_len = dict()

    def new_tcp_client(self, passive_sock, mask):
        client_sock, client_addr = passive_sock.accept()
        logging.info('New client connection from {} port {}'.format(*client_addr))
        # TODO: check that client did not already exist
        self.clients[client_sock] = client_addr
        self.select.register(client_sock, selectors.EVENT_READ, self.peer_read_tcp)

    def disconnect_client(self, client_socket):
        if client_socket not in self.clients:
            # Already done
            return
        client_addr = self.clients[client_socket]
        logging.info('Disconnecting client %s', client_addr)
        del self.clients[client_socket]
        if client_socket in self.client_msg:
            del self.client_msg[client_socket]
            del self.client_msg_len[client_socket]

    def peer_read_tcp(self, sock, mask):
        msg = self.client_msg.get(sock, None)
        if msg == None:
            # New message
            try:
                header = sock.recv(2)
            except socket.error as e:
                logging.error('Got "{}" while reading from client socket (bitmask was {})'.format(e, mask))
                self.disconnect_client(sock)
                return
            if not header:
                self.disconnect_client(sock)
                return
            self.client_msg_len[sock] = struct.unpack("!H", header)[0]
            self.client_msg[sock] = b""
        # Continue receiving a message
        data = sock.recv(self.client_msg_len[sock] - len(self.client_msg[sock]))
        if not data:
            self.disconnect_client(sock)
            return
        self.client_msg[sock] += data
        # Finished receiving a message
        if len(self.client_msg[sock]) == self.client_msg_len[sock]:
            self.handle_peer_msg(self.client_msg[sock], self.clients[sock][0])
            self.client_msg[sock] = None
            self.client_msg_len[sock] = 0

    def send_msg_to_peers(self, msg):
        # Encoding: 2 bytes with length in network bytes order, then data
        length = struct.pack("!H", len(msg))
        logging.debug("Forwarding to {} peers".format(len(self.clients)))
        # Send to all clients
        for client_sock in self.clients:
            client_sock.send(length + msg)


def main():
    loglevel = logging.INFO
    if len(sys.argv) > 1 and sys.argv[1] == "-v":
        loglevel = logging.DEBUG
    logging.basicConfig(format=LOGGING_FORMAT, level=loglevel, datefmt=LOGGING_DATEFMT)
    proxy = SimpleProxyServer()
    proxy.main_loop()

if __name__ == '__main__':
    main()
