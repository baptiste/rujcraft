#!/usr/bin/env python3

import sys


GAME_NAME = "Coucou tu veux voir ma partie ?"
GAME_VERSION = [25,26,27]
W3_PORT = 6112
GAME = ["3RAW", "PX3W"]

# This is just copy-pasted, because I don't know how to decode it...
# It contains at least the map name and the hosting player name.
STAT = b'\x01\x03I\x07\x01\x01k\x01\xd1k\x01Y\xa51\xabM\xcbaqs]Eow\x19omoae]M\x1fegioo!UyE!Mega!\xd33/7c/w3Qy\x01{o{o\x01\xed\x01\xede{\xdfOI\xb1\xd1W\xcd+\xd1\xbb\xe5o77\x13\x97\x17+\xc7\x00\n\x00\x00\x00\x01\x00\x00\x00\x01\x00\x00\x00\x08\x00\x00\x006\x01\x00\x00\xe0\x17'


def main(source_ip):
    if len(GAME_NAME) > 31:
        print("Game name too long!")
        exit(1)
    from scapy.all import Ether, IP, UDP, send, sendp, sr, srp
    from war3scapy import War3GameInfo, War3CreateGame, War3RefreshGame
    basepkt = Ether(dst="ff:ff:ff:ff:ff:ff")/IP(src=source_ip, dst="255.255.255.255")/UDP(sport=W3_PORT, dport=W3_PORT)
    creategame = War3CreateGame(version=GAME_VERSION,
                                product=GAME)
    refreshgame = War3RefreshGame()
    gameinfo = War3GameInfo(name=GAME_NAME,
                            version=GAME_VERSION,
                            product=GAME,
                            stat=STAT)
    #pkts = [basepkt/creategame, basepkt/gameinfo, basepkt/refreshgame]
    pkts = [basepkt/gameinfo]
    sendp(pkts, loop=1000, inter=0.25)

def usage():
    print("usage: sudo {} <source IP>".format(sys.argv[0]))
    print("Send spoofed Warcraft III game info packets on the local network.")
    print("You can choose any source IP address, and the Warcraft III client will try to connect to this IP address for joining the game.")

if __name__ == '__main__':
    if len(sys.argv) < 2:
        usage()
        exit(-1)
    main(sys.argv[1])
