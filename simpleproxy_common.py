import logging
import selectors
import socket
import struct

from scapy.all import Ether, IP, UDP, send, sendp, AsyncSniffer

MAGIC = 0xf7
META_PORT = 5555
WAR3_PORT = 6112
WAR3_PRODUCTS = [b"3RAW", b"PX3W"]
WAR3_VERSIONS = [25, 26, 27, 28]

LOGGING_FORMAT = '%(asctime)s [%(levelname)s] %(message)s'
#LOGGING_DATEFMT = "%Y-%m-%d %H:%M:%S"
LOGGING_DATEFMT = "%H:%M:%S"

# From https://bnetdocs.org/packet/434/w3gs-searchgame
MSG_TYPES = {
    0x2f: 'W3GS_SEARCHGAME',
    0x30: 'W3GS_GAMEINFO',
    0x31: 'W3GS_CREATEGAME',
    0x32: 'W3GS_REFRESHGAME',
    0x33: 'W3GS_DECREATEGAME',
}

# BBH prefix: MAGIC, TYPE, SIZE (in bytes, including header)
# - product is either "WAR3" or "W3XP" (Frozen Throne)
# - version is 26 for 1.26, 27 for 1.27, etc
# - game ID is incremented each time a new game is created
MSG_FMT = {
    # SEARCHGAME: product, version, 0
    0x2f: "=BBH4sII",
    # GAMEINFO: product, version, game ID, unknown, [variable-length data follows]
    0x30: "=BBH4sIII",
    # CREATEGAME: product, version, game ID
    0x31: "=BBH4sII",
    # REFRESHGAME: game ID, nb players, nb slots
    0x32: "=BBHIII",
    # DECREATEGAME: game ID
    0x33: "=BBHI",
}

def build_msg(msg_type, *args):
    fmt = MSG_FMT[msg_type]
    size = struct.calcsize(fmt)
    return struct.pack(fmt, MAGIC, msg_type, size, *args)

class SimpleProxy(object):

    def __init__(self):
        # Prepare select loop
        self.select = selectors.DefaultSelector()
        # UDP socket to reply to local Warcraft III games.  We don't bind
        # to a particular port, because listening to Warcraft III packets
        # is done by sniffing, not with this socket.
        self.local_sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        #self.local_sock.bind(('0.0.0.0', 0))
        self.select.register(self.local_sock, selectors.EVENT_READ, self.local_read_udp)
        # Setup packet capture to catch War3 messages.  This is because we
        # can't bind on the same port as Warcraft III if we run on the
        # same machine...
        # TODO: is it safe to call a callback like this from a different
        # thread?
        self.sniffer = AsyncSniffer(filter="udp src and dst port {}".format(WAR3_PORT),
                                    prn=self.local_sniff_udp)

    def handle_peer_msg(self, msg, peer_ip):
        msg_type = msg[1]
        msg_name = MSG_TYPES.get(msg_type, "")
        logging.debug("Received message of length {} from {}: 0x{:x} {}".format(len(msg), peer_ip, msg_type, msg_name))
        if msg_type == 0x2f:
            # SEARCHGAME: send it as broadcast without spoofing
            logging.debug("Broadcasting on local network")
            self.send_local_broadcast(msg)
        if msg_type in (0x30, 0x31, 0x32, 0x33):
            # All other messages: send them with IP spoofing
            logging.debug("Broadcasting on local network with spoofed IP address %s", peer_ip)
            self.send_local_broadcast(msg, spoofed_ip=peer_ip)

    def send_msg_to_peers(self, msg):
        # This method is overridden by client and server
        pass

    def local_sniff_udp(self, pkt):
        """Called by scapy when a UDP packet with the war3 port is sniffed.
        """
        logging.debug("Sniffing incoming UDP packet")
        src_ip = pkt["IP"].src
        src_port = pkt["UDP"].sport
        data = pkt["UDP"].load
        return self.parse_udp_message((src_ip, src_port), data)
        if data[0] != MAGIC:
            return

    def local_read_udp(self, sock, mask):
        """Called when we receive data on the local UDP socket (reply from a query
        we sent to war3)
        """
        data, source = sock.recvfrom(1500)
        self.parse_udp_message(source, data)

    def parse_udp_message(self, source, data):
        """Source is a pair (IP, UDP port)
        Data is a bytestring
        """
        if data[0] != MAGIC:
            return
        msg_type = data[1]
        msg_name = MSG_TYPES.get(msg_type, "")
        logging.debug("Received war3 message 0x{:x} {} from {}".format(msg_type, msg_name, source))
        if msg_type in (0x2f, 0x30, 0x33):
            # Simply inform our peer
            self.send_msg_to_peers(data)
        if msg_type == 0x31 or msg_type == 0x32:
            # A game has been created: reply so we get detailed GAMEINFO
            logging.debug("Replying with SEARCHGAME")
            fmt = MSG_FMT.get(msg_type)
            parsed = struct.unpack(fmt, data)
            # CREATEGAME has information on product (war3 or tft) and version
            if msg_type == 0x31:
                products = (parsed[3], )
                versions = (parsed[4], )
            else:
                products = WAR3_PRODUCTS
                versions = WAR3_VERSIONS
            for version in versions:
                for product in products:
                    reply = build_msg(0x2f, product, version, 0)
                    self.local_sock.sendto(reply, source)

    def send_local_broadcast(self, data, spoofed_ip=None):
        if spoofed_ip:
            bcast_pkt = Ether(dst="ff:ff:ff:ff:ff:ff")/IP(src=spoofed_ip, dst="255.255.255.255")/UDP(sport=WAR3_PORT, dport=WAR3_PORT)
            loopback_pkt = IP(src=spoofed_ip, dst="127.0.0.1")/UDP(sport=WAR3_PORT, dport=WAR3_PORT)
        else:
            # Get source port of local UDP socket, to make sure we receive replies
            local_port = self.local_sock.getsockname()[1]
            bcast_pkt = Ether(dst="ff:ff:ff:ff:ff:ff")/IP(dst="255.255.255.255")/UDP(sport=local_port, dport=WAR3_PORT)
            loopback_pkt = IP(dst="127.0.0.1")/UDP(sport=local_port, dport=WAR3_PORT)
        # Simply add the message as payload to the UDP packet
        # TODO: handle OSError
        sendp(bcast_pkt/data, verbose=False)
        send(loopback_pkt/data, verbose=False)

    def main_loop(self):
        logging.info("Entering main loop")
        self.sniffer.start()
        while True:
            events = self.select.select()
            for key, mask in events:
                if mask & selectors.EVENT_READ == 0:
                    logging.warning("Socket not readable, bitmask is {}".format(mask))
                callback = key.data
                callback(key.fileobj, mask)
